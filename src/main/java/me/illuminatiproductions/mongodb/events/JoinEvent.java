package me.illuminatiproductions.mongodb.events;

import me.illuminatiproductions.mongodb.MongoDB;
import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinEvent implements Listener {

    MongoDB plugin;

    public JoinEvent(MongoDB plugin) {
        this.plugin = plugin;
    }

    //Track the number of times someone has joined the server
    @EventHandler
    public void joinEvent(PlayerJoinEvent e){

        Player player = e.getPlayer();

        //Check to see if that player is already stored in the database
        Document playerDocument = new Document("name", player.getDisplayName()); //Create a "filter" document
        //Use the document we just created to see it exists in the database already
        Document result = plugin.collection.find(playerDocument).first(); //First result or null if not found
        if (result == null){ //It will be null if the player was not found
            //Since the player doesnt already exist, let's create a document for the player in the server

            //Create a new document to represent the player(just use the filter doc)
            playerDocument.append("joins", 1); //Store the # of times they have joined the server. 1 Since this is the first time
            //That's all the data we will track for now so let's add it to the collection
            plugin.collection.insertOne(playerDocument);
        }else{  //Found the player already in the database
            //Since they are on the database already, we simply need to increase their join number

            int joins = result.getInteger("joins") + 1; //Get current number and add 1
            //Update the document
            Document updatedDocument = new Document("name", result.getString("name"))
                    .append("joins", joins);
            plugin.collection.replaceOne(result, updatedDocument); //Document updated
        }


    }

}
