package me.illuminatiproductions.mongodb.commands;

import me.illuminatiproductions.mongodb.MongoDB;
import org.bson.Document;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JoinsCommand implements CommandExecutor {

    MongoDB plugin;

    public JoinsCommand(MongoDB plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //Allow a player to see how many times they have joined the server
        if (sender instanceof Player){
            Player player = (Player) sender;
            //Search the database for their name
            Document filter = new Document("name", player.getDisplayName());
            Document playerDocumentResult = plugin.collection.find(filter).first(); //Player document
            //get the number of joins from the document
            int joins = playerDocumentResult.getInteger("joins");
            player.sendMessage("You have joined " + joins + " times.");
        }
        return true;
    }
}
