package me.illuminatiproductions.mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import me.illuminatiproductions.mongodb.commands.JoinsCommand;
import me.illuminatiproductions.mongodb.events.JoinEvent;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class MongoDB extends JavaPlugin {

    public MongoCollection<Document> collection;

    @Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("Plugin has started up");

        //register the event
        Bukkit.getServer().getPluginManager().registerEvents(new JoinEvent(this), this);

        //register command
        getCommand("joins").setExecutor(new JoinsCommand(this));

        //Connect to the database when the plugin starts up
        MongoClient mongoClient = MongoClients.create("mongodb://kody:password123@ds053954.mlab.com:53954/spigot");
        collection = mongoClient.getDatabase("spigot").getCollection("users");

        System.out.println("Connected to database");
    }

}
